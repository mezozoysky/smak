#include <Smak/Engine/App.hpp>
#include <Smak/StringHash.hpp>
#include <Smak/Engine/Log.hpp>
#include <fmt/format.h>

using Smak::Engine::Log;
using Smak::StringHash;

using namespace fmt::literals;

class App : public Smak::Engine::App
{
public:
    App() = default;
    virtual ~App() noexcept = default;

protected:
    virtual void onInit() override final {};
    virtual void onUninit() override final {};
    virtual void onStart() override final
    {
        StringHash hashZasada0(std::string("Zasada"));
        StringHash hashZasada1(std::string("Zasada"));
        StringHash hashZasada2(std::string("Zasada"));
        StringHash hashZasada3(std::string("Zasada"));
        StringHash hashZasada4(std::string("zasada"));
        StringHash hashZasada5(sf::String("Zasada"));
        StringHash hashZasada51(sf::String("Zasada"));
        StringHash hashZasada6(sf::String("zasada"));
        StringHash hashZasada61(sf::String("zasada"));

        StringHash hashEmpty(std::string(""));
        StringHash hashEmptySF(sf::String(""));
        StringHash hash_(std::string("_"));
        StringHash hash_SF(sf::String("_"));

        auto log = context()->getSystem<Log>();
        log->log<Log::INFO>("hash from 'Zasada': '{}'"_format(hashZasada0.value()));
        log->log<Log::INFO>("hash from 'Zasada': '{}'"_format(hashZasada1.value()));
        log->log<Log::INFO>("hash from 'Zasada': '{}'"_format(hashZasada2.value()));
        log->log<Log::INFO>("hash from 'Zasada': '{}'"_format(hashZasada3.value()));
        log->log<Log::INFO>("hash from 'zasada': '{}'"_format(hashZasada4.value()));
        log->log<Log::INFO>("sf hash from 'Zasada': '{}'"_format(hashZasada5.value()));
        log->log<Log::INFO>("sf hash from 'Zasada': '{}'"_format(hashZasada51.value()));
        log->log<Log::INFO>("sf hash from 'zasada': '{}'"_format(hashZasada6.value()));
        log->log<Log::INFO>("sf hash from 'zasada': '{}'"_format(hashZasada61.value()));

        log->log<Log::INFO>("hash from '': '{}'"_format(hashEmpty.value()));
        log->log<Log::INFO>("sf hash from '': '{}'"_format(hashEmptySF.value()));
        log->log<Log::INFO>("hash from '_': '{}'"_format(hash_.value()));
        log->log<Log::INFO>("sf hash from '_': '{}'"_format(hash_SF.value()));
    };
    virtual void onRender() override final {};
};
