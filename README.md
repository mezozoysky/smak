# Smak #
Simple multimedia app kit

## Status ##
Just starting

## License ##
*Smak* is released under the terms of zlib/png license;
see full license text in LICENSE.md file or at https://opensource.org/licenses/Zlib

## Build ##
Nothing to build

## Install ##
Nothing to install

## Documentation ##
Nothing to document
