#pragma once

#include <type_traits>

namespace sf
{
    class Time;
}

namespace Smak::Gui
{

class Gui
{
public:
    Gui() = default;
    virtual ~Gui() = default;

    Gui(const Gui&) = delete;
    const Gui& operator=(const Gui&) = delete;

    virtual void setUp() = 0;
    virtual void tearDown() = 0;
    virtual void update(const sf::Time& deltaTime) = 0;
    virtual void render() = 0;
};

template<typename GuiImplT>
class GuiBase : public Gui
{
public:
    GuiBase()
    : Gui{}
    {
    }
    virtual ~GuiBase() = default;
};

} // namespace Smak::Gui
