#pragma once

#include <limits>
#include <type_traits>
#include <string>
#include <ctime>

namespace Smak::Log
{

class LogLevel
{
public:
    LogLevel() = delete;
    inline explicit LogLevel(std::size_t p, const std::string& name) noexcept
    : m_priority{p}
    , m_name{name}
    {
    }
    ~LogLevel() = default;

    inline const std::size_t priority() const noexcept { return m_priority; }

    inline const std::string& name() const noexcept { return m_name; }

private:
    std::size_t m_priority;
    std::string m_name;
};


struct LogMessage
{
    const std::string message;
    const std::time_t time;
    const std::string tag;
    const LogLevel& level;

    inline LogMessage(const std::string& message,
               const std::time_t& time,
               const std::string& tag,
               const LogLevel& level) noexcept
    : message{message}
    , time{time}
    , tag{tag}
    , level{level}
    {
    }
};

class LoggerFormat
{
public:
    LoggerFormat() = default;
    ~LoggerFormat() = default;
};

class LoggerDestination
{
public:
    LoggerDestination() = default;
    ~LoggerDestination() = default;
};

class Logger
{
public:
    Logger() = default;
    virtual ~Logger() = default;

    Logger(const Logger&) = delete;
    const Logger& operator=(const Logger&) = delete;

    virtual void logMessage(const LogMessage& message) = 0;

    template<const LogLevel& levelV>
    inline void log(const std::string& text)
    {
        logMessage(LogMessage{text, getTime(), tag(), levelV});
    }

    virtual const std::string& tag() const noexcept = 0;
    virtual const LogLevel& level() const noexcept = 0;

    virtual const LoggerFormat& format() const noexcept = 0;
    virtual const LoggerDestination& destination() const noexcept = 0;

    virtual void setLevel(const LogLevel& level) noexcept = 0;

protected:
    virtual const Logger* getParent() const noexcept = 0;
friend class Log;
    virtual const std::time_t getTime() const noexcept = 0;
};


class Log : public Logger
{
public:
    static const LogLevel HEALTH;

protected:
    static const std::string EMPTY_LOG_TAG;

public:
    Log() = default;
    virtual ~Log() = default;

    virtual void setUp() = 0;
    virtual void tearDown() = 0;

    virtual Logger* const logger(const std::string& tag) noexcept = 0;

    virtual const Logger* const cRootLogger() const noexcept = 0;
    virtual Logger* const rootLogger() noexcept = 0;

    inline virtual void logMessage(const LogMessage& message) override final
    {
        rootLogger()->logMessage(message);
    }

    inline virtual const std::string& tag() const noexcept override final { return EMPTY_LOG_TAG; }
    inline virtual const LogLevel& level() const noexcept override final { return cRootLogger()->level(); }

    inline virtual const LoggerFormat& format() const noexcept override final
    {
        return cRootLogger()->format();
    }
    inline virtual const LoggerDestination& destination() const noexcept override final
    {
        return cRootLogger()->destination();
    }

    inline virtual void setLevel(const LogLevel& level) noexcept override final
    {
        rootLogger()->setLevel(level);
    }

protected:
    inline virtual const std::time_t getTime() const noexcept override final
    {
        return cRootLogger()->getTime();
    }

    inline virtual const Logger* getParent() const noexcept override final { return nullptr; }
};


template<typename LogImplT>
class LogBase : public LogImplT
{
public:
    LogBase()
    : LogImplT{}
    {
        static_assert(std::is_base_of<Log, LogImplT>(),
                      "Log implementation must be subclass of Smak::Render::Render");
    }
    virtual ~LogBase() = default;
};

} // namespace Smak::Log

// context().get_system<Log>()->get_logger("Render
