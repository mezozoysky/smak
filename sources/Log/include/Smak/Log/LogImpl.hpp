#pragma once

#include <unordered_set>
#include <memory>

#include "Log.hpp"

namespace Smak::Log
{

class LoggerImpl : public Logger
{
public:
    LoggerImpl(LoggerImpl* parent, const std::string& tag);
    virtual ~LoggerImpl();

    virtual void logMessage(const LogMessage& message) override final;

    inline virtual const Logger* getParent() const noexcept override final { return mParent; }
    inline virtual const std::string& tag() const noexcept override final { return mTag; }
    inline virtual const LogLevel& level() const noexcept override final { return mLevel; }

    inline virtual const LoggerFormat& format() const noexcept override final { return mFormat; }
    inline virtual const LoggerDestination& destination() const noexcept override final
    {
        return mDestination;
    }

    virtual void setLevel(const LogLevel& level) noexcept override final
    {
        mLevel = level;
    }

protected:
    virtual const std::time_t getTime() const noexcept override final;

private:
    LoggerImpl* mParent;
    std::string mTag;
    LogLevel mLevel;

    LoggerFormat mFormat;
    LoggerDestination mDestination;
};


class LogImpl : public Log
{
public:
    LogImpl();
    virtual ~LogImpl();

    virtual void setUp() override;
    virtual void tearDown() override;

    virtual Logger* const logger(const std::string& tag) noexcept override final;

    inline virtual const Logger* const cRootLogger() const noexcept override final { return &mRootLogger; }
    inline virtual Logger* const rootLogger() noexcept override final { return &mRootLogger; }

private:
    LoggerImpl mRootLogger;
    std::unordered_set<std::unique_ptr<LoggerImpl>> mLoggers;
};

} // namespace Smak::Log
