project(Log LANGUAGES C CXX)

add_library(${PROJECT_NAME} STATIC)
add_library(Smak::Log ALIAS ${PROJECT_NAME})

find_package(fmt 5
    CONFIG REQUIRED)

set(PROJECT_HEADERS_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/include/")
set(PROJECT_SOURCES_ROOT "${CMAKE_CURRENT_SOURCE_DIR}/src/")

set(PROJECT_HEADERS
    ${PROJECT_HEADERS_ROOT}/Smak/Log/Log.hpp
    ${PROJECT_HEADERS_ROOT}/Smak/Log/LogImpl.hpp
    )
set(PROJECT_SOURCES
    ${PROJECT_SOURCES_ROOT}/Smak/Log/Log.cpp
    ${PROJECT_SOURCES_ROOT}/Smak/Log/LogImpl.cpp
    )
source_group(TREE "${PROJECT_HEADERS_ROOT}" FILES ${PROJECT_HEADERS})
source_group(TREE "${PROJECT_SOURCES_ROOT}" FILES ${PROJECT_SOURCES})

### LIBRARY
set_target_properties(${PROJECT_NAME}
    PROPERTIES
        LINKER_LANGUAGE CXX
        OUTPUT_NAME Smak${PROJECT_NAME}
    )
target_sources(${PROJECT_NAME}
    PRIVATE
        ${PROJECT_SOURCES}
    )
target_link_libraries(${PROJECT_NAME}
    INTERFACE
        Smak::Core
        fmt::fmt
    )
target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<BUILD_INTERFACE:${PROJECT_HEADERS_ROOT}>
        $<INSTALL_INTERFACE:include/>
    )
target_include_directories(${PROJECT_NAME}
    PUBLIC
        $<TARGET_PROPERTY:Smak::Core,INTERFACE_INCLUDE_DIRECTORIES>
    )
target_include_directories(${PROJECT_NAME}
    PRIVATE
        $<TARGET_PROPERTY:fmt::fmt,INTERFACE_INCLUDE_DIRECTORIES>
    )
#target_compile_definitions(${PROJECT_NAME})
#target_compile_option(${PROJECT_NAME})

include(GNUInstallDirs)
install(
    TARGETS ${PROJECT_NAME}
    EXPORT SmakTargets
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    )
install(
    DIRECTORY ${PROJECT_HEADERS_ROOT}
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
    )

if(Smak_BUILD_UT)
    #add_subdirectory(unit_tests)
endif(Smak_BUILD_UT)
