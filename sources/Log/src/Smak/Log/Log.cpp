#include <Smak/Log/Log.hpp>

namespace Smak::Log
{

const LogLevel Log::HEALTH{0, "HEALTH"};
const std::string Log::EMPTY_LOG_TAG{};


} // namespace Smak::Log
