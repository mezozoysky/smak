#include <algorithm>
#include <cassert>
#include <fmt/core.h>
#include <Smak/Log/LogImpl.hpp>

namespace Smak::Log
{

//
// LoggerImpl
//

LoggerImpl::LoggerImpl(LoggerImpl* parent, const std::string& tag)
: Logger{}
, mParent{parent}
, mTag{tag}
, mLevel{parent ? parent->level() : Log::HEALTH}
{
}

LoggerImpl::~LoggerImpl() {}

void LoggerImpl::logMessage(const LogMessage& message)
{
    if (message.level.priority() > mLevel.priority())
    {
        return;
    }
    std::tm tm = *std::localtime(&message.time);
    fmt::print("{:4d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}|{}|{}|: {}\n",
               tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec,
               message.level.name(),
               message.tag,
               message.message);
}

const std::time_t LoggerImpl::getTime() const noexcept
{
    return time(0);
}

//
// LogImpl
//

LogImpl::LogImpl()
: Log{}
, mRootLogger{nullptr, EMPTY_LOG_TAG}
{
}

LogImpl::~LogImpl() {}

void LogImpl::setUp() {}

void LogImpl::tearDown() {}

Logger* const LogImpl::logger(const std::string& tag) noexcept
{
    if (tag == EMPTY_LOG_TAG)
    {
        return &mRootLogger;
    }
    auto it = std::find_if(std::begin(mLoggers),
                           std::end(mLoggers),
                           [tag](const std::unique_ptr<LoggerImpl>& l){ return l->tag() == tag; });
    if (it == mLoggers.end())
    {
        auto [new_it, success] = mLoggers.emplace(std::make_unique<LoggerImpl>(&mRootLogger, tag));
        if (!success)
        {
            assert(false && "Failed to create logger by tag!");
            mRootLogger.log<Log::HEALTH>("Failed to create logger by tag\"" + tag + "\"");
            return &mRootLogger;
        }
        it = new_it;
    }
    return (it->get());
}

} // namespace Smak::Log
