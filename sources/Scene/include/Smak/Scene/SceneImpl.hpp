#pragma once

#include <cstdint>
#include <cstring>
#include <memory>

#include <SFML/Graphics.hpp>

#include "Scene.hpp"

namespace Smak::Scene
{

class SceneImpl
: public Scene
{
public:
    SceneImpl();
    virtual ~SceneImpl();

    virtual void setUp() override;
    virtual void tearDown() override;
};

} // namespace Smak::Scene
