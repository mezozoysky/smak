#pragma once

#include <type_traits>

namespace Smak::Scene
{

class Scene
{
public:
    Scene() = default;
    virtual ~Scene() = default;

    Scene(const Scene&) = delete;
    const Scene& operator=(const Scene&) = delete;

    virtual void setUp() = 0;
    virtual void tearDown() = 0;
};

template<typename SceneImplT>
class SceneBase : public SceneImplT
{
public:
    SceneBase()
    : SceneImplT{}
    {
        static_assert(std::is_base_of<Scene, SceneImplT>(),
                      "Scene implementation must be subclass of Smak::Scene::Scene");
    }
    virtual ~SceneBase() = default;
};

} // namespace Smak::Scene
