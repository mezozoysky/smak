#include <Smak/Context.hpp>

namespace Smak
{

void Context::addSystem(std::type_index type_index, ContextualBase* contextual)
{
    if (mSystems.count(type_index))
    {
        if (contextual == nullptr)
        {
            mSystems.erase(type_index);
        }
        else
        {
            mSystems[type_index].reset(contextual);
        }
    }
    else
    {
        mSystems.emplace(type_index, contextual);
    }
}

void Context::dropSystem(std::type_index type_index)
{
    if (mSystems.count(type_index))
    {
        mSystems[type_index].reset();
        mSystems.erase(type_index);
    }
}

ContextualBase* Context::getSystem(std::type_index type_index)
{
    if (mSystems.count(type_index))
    {
        return mSystems.at(type_index).get();
    }
    return nullptr;
}


} // namespace Smak
