#include <Smak/StringHash.hpp>
#include <xxhash.h>

namespace Smak
{

unsigned int hashData32(const void* data, std::size_t length, unsigned int seed = 0u)
{
    return XXH32(data, length, seed);
}

unsigned int StringHash::seed = 0u;

constexpr StringHash::StringHash() noexcept
: mValue(0)
{
}

constexpr StringHash::StringHash(const unsigned int& value) noexcept
: mValue(value)
{
}

StringHash::StringHash(const std::string& str)
{
    mValue = hashData32(static_cast<const void*>(str.data()), str.size(), seed);
}

StringHash::StringHash(const std::string_view& str)
{
    mValue = hashData32(static_cast<const void*>(str.data()), str.size(), seed);
}

} // namespace Smak
