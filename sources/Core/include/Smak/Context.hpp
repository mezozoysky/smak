#pragma once

#include <variant>
#include <unordered_map>
#include <string>
#include <typeindex>
#include <memory>

namespace Smak
{

class ContextualBase;

//
// Context
//

class Context
{
public:
    using Variant = std::variant<int, long, float, double, const char*, std::string>;
    using VariantMap = std::unordered_map<std::string, Variant>;

    using TypeMap = std::unordered_map<std::type_index, std::shared_ptr<ContextualBase> >;

public:
    Context() = default;
    virtual ~Context() = default;

    Context(const Context&) = delete;
    const Context& operator=(const Context&) = delete;

    template<typename T>
    void setValue(const std::string& name, const T& value)
    {
        mValues[name] = value;
    }
    template<typename T>
    const T& value(const std::string& name) const
    {
        return std::get<T>(mValues.at(name));
    }

    void addSystem(std::type_index type_index, ContextualBase* contextual);
    template<typename SystemT>
    inline void addSystem(SystemT* contextual)
    {
        addSystem(std::type_index(typeid(SystemT)), contextual);
    }

    void dropSystem(std::type_index type_index);
    template<typename SystemT>
    inline void dropSystem()
    {
        dropSystem(std::type_index(typeid(SystemT)));
    }

    ContextualBase* getSystem(std::type_index type_index);
    template<typename SystemT>
    inline SystemT* getSystem()
    {
        return static_cast<SystemT*>(getSystem(std::type_index(typeid(SystemT))));
    }

private:
    VariantMap mValues;
    TypeMap mSystems;
};


//
// Contextual
//

class ContextualBase
{
protected:
    ContextualBase() = default;

public:
    virtual ~ContextualBase() = default;
};

template<typename DerivedT, typename ContextT>
class Contextual
: public ContextualBase
{
public:
    using ContextType = ContextT;

public:
    Contextual(ContextT* const context)
    : mContext{context}
    {
    }

    virtual ~Contextual() = default;

protected:
    inline ContextT* const context() const
    {
        return mContext;
    }

private:
    ContextT* const mContext;
};

} // namespace Smak
