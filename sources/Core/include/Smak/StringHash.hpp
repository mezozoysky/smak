#pragma once

#include <SFML/Config.hpp>
#include <string>

namespace Smak
{

unsigned int hashData32(const void* data, std::size_t length);

class StringHash
{
public:
    static unsigned int seed;

public:
    constexpr StringHash() noexcept;
    constexpr explicit StringHash(const unsigned int& value) noexcept;

    StringHash(const std::string& str);
    StringHash(const std::string_view& str);

    inline constexpr bool isValid() const
    {
        return (mValue != 0);
    }
    inline constexpr explicit operator bool() const
    {
        return isValid();
    }

    inline unsigned int value() const noexcept
    {
        return mValue;
    }

private:
    unsigned int mValue;
};

} // namespace Smak

