#pragma once

namespace Smak
{

class App
{
public:
    App() = default;
    virtual ~App() noexcept = default;

    App(const App&) = delete;
    const App& operator=(const App&) = delete;

    virtual void run() = 0;
};

template<typename DerivedAppT, typename ContextT>
class AppBase : public App
{
public:
    using ContextType = ContextT;
    AppBase() = default;
    virtual ~AppBase() = default;

    inline ContextT* const context() const noexcept;
private:
    ContextT mContext;
};

template<typename DerivedAppT, typename ContextT>
inline ContextT* const AppBase<DerivedAppT, ContextT>::context() const noexcept
{
    return (ContextT* const)(&mContext);
}

} // namespace Smak
