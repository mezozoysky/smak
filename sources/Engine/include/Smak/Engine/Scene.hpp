#pragma once

#include <Smak/Scene/SceneImpl.hpp>
#include "Context.hpp"

namespace Smak::Engine
{

class Scene
: public Smak::Scene::SceneBase<Smak::Scene::SceneImpl>
, public Contextual<Scene>
{
public:
    Scene(Context* const context);
    virtual ~Scene();
};

} // namespace Smak::Engine
