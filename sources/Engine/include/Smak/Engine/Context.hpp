#pragma once

#include <SFML/Graphics.hpp>
#include <Smak/Context.hpp>

namespace Smak::Engine
{

class Context : public Smak::Context
{
public:
    Context() = default;
    virtual ~Context() = default;

    inline sf::RenderWindow& window() noexcept;

private:
    sf::RenderWindow mWindow;
};

inline sf::RenderWindow& Context::window() noexcept
{
    return mWindow;
}


template<typename ContextualT>
class Contextual : public Smak::Contextual<ContextualT, Context>
{
public:
    inline Contextual(Context* const context)
    : Smak::Contextual<ContextualT, Context>(context)
    {
    }
    virtual ~Contextual() = default;
};

} // namespace Smak::Engine
