#pragma once

#include <Smak/App.hpp>
#include "Context.hpp"

namespace Smak::Engine
{

class App : public Smak::AppBase<App, Context>
{
public:
    App();
    virtual ~App();

    virtual void run() override final;

protected:
    void init();
    void uninit();
    void loop();

    virtual void onInit() = 0;
    virtual void onUninit() = 0;
    virtual void onStart() = 0;
    virtual void onRender() = 0;

private:
};

} // namespace Smak::Engine
