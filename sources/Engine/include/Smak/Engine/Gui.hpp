#pragma once

#include <Smak/Gui/Gui.hpp>
#include "Context.hpp"
//#include "Render.hpp"

namespace Smak::Engine
{

class Gui
: public Smak::Gui::GuiBase<Gui>
, public Contextual<Gui>
{
public:
    Gui(Context* const context);
    virtual ~Gui();

    virtual void setUp() override final;
    virtual void tearDown() override final;
    virtual void update(const sf::Time& deltaTime) override final;
    virtual void render() override final;

    void handleEvent(sf::Event& event);

};


} // namespace Smak::Engine
