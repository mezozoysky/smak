#pragma once

#include <variant>
#include <unordered_map>

namespace Smak::Engine
{

using Variant = std::variant<int, long, float, double, const char*, std::string>;
using VariantMap = std::unordered_map<std::string, Variant>;

} // namespace Smak::Engine
