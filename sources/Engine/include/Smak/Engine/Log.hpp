#pragma once

#include <Smak/Log/LogImpl.hpp>
#include "Context.hpp"


using Smak::Log::LogLevel;

namespace Smak::Engine
{

class Log
: public Smak::Log::LogBase<Smak::Log::LogImpl>
, public Contextual<Log>
{
public:
    static const LogLevel FATAL;
    static const LogLevel ERROR;
    static const LogLevel WARNING;
    static const LogLevel INFO;
    static const LogLevel DEBUG;

public:
    Log(Context* const context);
    virtual ~Log();
};

} // namespace Smak::Engine
