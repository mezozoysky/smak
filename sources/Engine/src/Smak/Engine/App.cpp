#include <SFML/System.hpp>

#include <Smak/Engine/App.hpp>
#include <Smak/Engine/Log.hpp>
#include <Smak/Engine/Scene.hpp>
#include <Smak/Engine/Gui.hpp>

using Smak::Log::LogLevel;

namespace Smak::Engine
{

App::App()
: Smak::AppBase<App, Context>{}
{
    context()->window().create(sf::VideoMode(1024, 768, 32), L"Smak");
}

App::~App() {}

void App::run()
{
    init();
    onInit();
    onStart();
    loop();
    onUninit();
    uninit();
}

void App::init()
{
    Log* log = new Log(context());
    log->setLevel(Log::INFO);
    log->setUp();
    log->log<Log::INFO>("Start logging!");
    context()->addSystem(log);
    // TODO: load config/parse command line/etc
    Gui* gui = new Gui(context());
    gui->setUp();
    context()->addSystem(gui);
}

void App::uninit()
{
    context()->getSystem<Gui>()->tearDown();
    context()->dropSystem<Gui>();
    context()->getSystem<Log>()->tearDown();
    context()->dropSystem<Log>();
}

void App::loop()
{
    sf::Clock timer;
    sf::Time elapsed = sf::Time::Zero;
    sf::RenderWindow& window = context()->window();
    window.resetGLStates();

    Gui* gui = context()->getSystem<Gui>();

    sf::Event event;
    while (window.isOpen())
    {
        elapsed = timer.restart();
        while (window.pollEvent(event))
        {
            gui->handleEvent(event);
            switch (event.type)
            {
            case sf::Event::Closed:
                window.close();
                break;
            case sf::Event::KeyPressed:
                if (event.key.code == sf::Keyboard::Escape)
                {
                    window.close();
                }
                break;
            default:
                break;
            }
        }
        if (window.isOpen())
        {
            gui->update(elapsed);
            window.clear(sf::Color::Magenta);
            onRender();
            gui->render();
            window.display();
        }
    }
}


} // namespace Smak::Engine
