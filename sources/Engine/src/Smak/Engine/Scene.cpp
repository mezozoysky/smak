#include <Smak/Engine/Scene.hpp>


namespace Smak::Engine
{

Scene::Scene(Context* const context)
: Smak::Scene::SceneBase<Smak::Scene::SceneImpl>()
, Contextual<Scene>(context)
{
}

Scene::~Scene()
{
}

} // namespace Smak::Engine
