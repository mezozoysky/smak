#include <Smak/Engine/Gui.hpp>

#include <imgui.h>
#include <imgui-SFML.h>


namespace Smak::Engine
{

Gui::Gui(Context* const context)
: Smak::Gui::GuiBase<Gui>()
, Contextual<Gui>(context)
{
}

Gui::~Gui() {}

void Gui::setUp()
{
    ImGui::SFML::Init(context()->window());
}

void Gui::tearDown()
{
    ImGui::SFML::Shutdown();
}

void Gui::update(const sf::Time& deltaTime)
{
    ImGui::SFML::Update(context()->window(), deltaTime);

    ImGui::Begin("ZASADA!");
    ImGui::Button("Imperium Button");
    ImGui::End();
}

void Gui::render()
{
    ImGui::SFML::Render(context()->window());
}

void Gui::handleEvent(sf::Event& event)
{
  ImGui::SFML::ProcessEvent(event);
}

} // namespace Smak::Engine
