#include <Smak/Engine/Log.hpp>

namespace Smak::Engine
{

const LogLevel Log::FATAL{1, "FATAL"};
const LogLevel Log::ERROR{2, "ERROR"};
const LogLevel Log::WARNING{3, "WARNING"};
const LogLevel Log::INFO{4, "INFO"};
const LogLevel Log::DEBUG{5, "DEBUG"};

Log::Log(Context* const context)
: Smak::Log::LogBase<Smak::Log::LogImpl>()
, Contextual<Log>(context)
{
}

Log::~Log() {}

} // namespace Smak::Engine
