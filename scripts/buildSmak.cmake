cmake_minimum_required(VERSION 3.13 FATAL_ERROR)

if(NOT CMAKE_SCRIPT_MODE_FILE OR CMAKE_PARENT_LIST_FILE)
    message(FATAL_ERROR "This file '${CMAKE_CURRENT_LIST_FILE}' should be used with 'cmake -P'")
endif()

get_filename_component(SCRIPT_NAME "${CMAKE_SCRIPT_MODE_FILE}" NAME)
get_filename_component(SCRIPTS_DIR "${CMAKE_SCRIPT_MODE_FILE}" DIRECTORY)
get_filename_component(PROJECT_ROOT_DIR "${SCRIPTS_DIR}" DIRECTORY)
set(SMAK_BUILD_DIR "${PROJECT_ROOT_DIR}/build")

message("Script file : ${CMAKE_SCRIPT_MODE_FILE}")
message("Script name : ${SCRIPT_NAME}")
message("Scripts dir : ${SCRIPTS_DIR}")
message("Project dir : ${PROJECT_ROOT_DIR}")
message("Build dir   : ${SMAK_BUILD_DIR}")

if(NOT SMAK_SFML_DIR)
    set(SMAK_SFML_DIR "${PROJECT_ROOT_DIR}/gitmodules/SFML/build")
endif()
if(NOT SMAK_IMGUI_SFML_DIR)
    set(SMAK_IMGUI_SFML_DIR "${PROJECT_ROOT_DIR}/gitmodules/imgui-sfml/local/lib/cmake/ImGui-SFML")
endif()
if(NOT SMAK_FMT_DIR)
    set(SMAK_FMT_DIR "${PROJECT_ROOT_DIR}/gitmodules/fmt/build")
endif()
if(NOT SMAK_XXHASH_DIR)
    set(SMAK_XXHASH_DIR "${PROJECT_ROOT_DIR}/gitmodules/xxHash/build")
endif()

message("SFML dir       : ${SMAK_SFML_DIR}")
message("ImGui-SFML dir : ${SMAK_IMGUI_SFML_DIR}")
message("fmt dir        : ${SMAK_FMT_DIR}")
message("xxHash dir     : ${SMAK_XXHASH_DIR}")

if(EXISTS "${SMAK_BUILD_DIR}")
    file(REMOVE "${SMAK_BUILD_DIR}/CMakeCache.txt")
    file(REMOVE_RECURSE "${SMAK_BUILD_DIR}/CMakeFiles")
else()
    file(MAKE_DIRECTORY "${SMAK_BUILD_DIR}")
endif()

message("Configuring ...")
execute_process(
    COMMAND "${CMAKE_COMMAND}" .. -DBUILD_SHARED_LIBS=OFF
                                  -DCMAKE_BUILD_TYPE=Debug
                                  -DSFML_STATIC_LIBRARIES=ON
                                  -DSFML_DIR=${SMAK_SFML_DIR}
                                  -DImGui-SFML_DIR=${SMAK_IMGUI_SFML_DIR}
                                  -Dfmt_DIR=${SMAK_FMT_DIR}
                                  -DxxHash_DIR=${SMAK_XXHASH_DIR}
    WORKING_DIRECTORY "${SMAK_BUILD_DIR}"
    RESULT_VARIABLE CONFIGURE_RESULT
    OUTPUT_FILE "${SCRIPTS_DIR}/buildSmak-configure.out"
    ERROR_FILE "${SCRIPTS_DIR}/buildSmak-configure.out"
    ENCODING UTF8)
message("Configure result: ${CONFIGURE_RESULT}")
if(NOT ${CONFIGURE_RESULT} EQUAL 0)
    message(FATAL_ERROR "Failed to configure: ${CONFIGURE_RESULT}")
endif()

message("Building ...")
execute_process(
    COMMAND cmake --build .
    WORKING_DIRECTORY "${SMAK_BUILD_DIR}"
    RESULT_VARIABLE BUILD_RESULT
    OUTPUT_FILE "${SCRIPTS_DIR}/buildSmak-build.out"
    ERROR_FILE "${SCRIPTS_DIR}/buildSmak-build.out"
    ENCODING UTF8)
message("Build result: ${BUILD_RESULT}")
if(NOT ${BUILD_RESULT} EQUAL 0)
    message(FATAL_ERROR "Failed to build: ${BUILD_RESULT}")
endif()

