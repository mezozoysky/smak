#!/bin/bash

#export CC=/usr/bin/clang
#export CXX=/usr/bin/clang++

scripts_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
project_dir="$scripts_dir/../"
gitmodules_dir="$project_dir/gitmodules/"
module_dir="$project_dir/gitmodules/fmt/"
[ -d "$module_dir" ] || {
    pushd $project_dir
        git submodule init
        git submodule update
    popd
}
[ -d "$module_dir" ] ||
{
    echo "$0 ERROR: failed to find git submodule within '$module_dir'" >&2
    exit 2
}
module_build_dir="$module_dir/build/"
[ -d "$module_build_dir" ] || mkdir -p "$module_build_dir"
[ -d "$module_build_dir" ] ||
{
    echo "$0 ERROR: failed to create submodule build directory '$module_build_dir'" >&2
    exit 2
}


pushd $module_build_dir
rm -rf ./*
cmake ..    -DCMAKE_BUILD_TYPE=Debug \
            -DFMT_INSTALL=ON \
|| {
    echo "$0 ERROR: failed to configure/generate build files"
    exit 128
}
cmake --build . --config Debug || {
    echo "$0 ERROR: failed to build"
}

popd

